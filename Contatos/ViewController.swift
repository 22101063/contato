//
//  ViewController.swift
//  Contatos
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

struct Contato {
    let nome: String
    let numero: String
    let email: String
    let endereco: String
}
class ViewController: UIViewController, UITableViewDataSource {
        
        @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        
        listadeContatos.append(Contato(nome: "Contato1", numero: "(31) 98989-0000", email: "Contato1@gmail.com", endereco: "Rua Cristal n1"))
        listadeContatos.append(Contato(nome: "Contato2", numero: "(31) 98989-1111", email: "Contato3@gmail.com", endereco: "Rua Cristal n2"))
        listadeContatos.append(Contato(nome: "Contato3", numero: "(31) 98989-2222", email: "Contato4@gmail.com", endereco: "Rua Cristal n3"))
    }
    
    
    var listadeContatos:[Contato] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listadeContatos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let contato = listadeContatos[indexPath.row]
        cell.nome.text = contato.nome
        cell.numero.text = contato.numero
        cell.email.text = contato.email
        cell.endereco.text = contato.endereco
        
        return cell
    }
    
    
        
    }




